import json
import os

def handler(event, context):
    print("spin and heave sample")
    print(event)
    slug = "no slug given"
    try:
        slug = event['pathParameters']['slug']
    except:
        pass
    return {
        'statusCode': 200,
        'body': json.dumps({'yup':slug}),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
    }
