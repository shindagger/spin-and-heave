# Sample Terraform and Lambda for Serverless   
  
Included is a sample python lambda project with terraform files with which you could Spin &amp; Heave. Once configured (see below), running Spin &amp; Heave will use a docker image to build and zip your lambda package (*spin*), then terraform will upload the deployment package to s3, create a lambda function and lambda role, a cloudwatch log group and policy attachment, an API gateway with usage plan and API key, two routes with method type GET, an API deployment, and permission for the API to invoke the lambda function (*heave*).  
  
If it works, it will output an API endpoint, and an API key.  
  
To test that everything is working try a version of the following:  
  
`$ curl -H "X-Api-Key:API_KEY" API_ENDPOINT/sample/thisworks`  
  
### Configuration of Example  
  
1. Create a `terraform.tfvars` file in this directory with the following contents  

    ```
    access_key = "aws_sdk_access_id"    
    secret_key = "aws_sdk_secret_key"
    ```  
  
2. Ensure that you are configured (in AWS) to the same account as the information above.  
  
3. Create a bucket in s3 for your terraform state, and lambda zip.  
  
4. Ensure that the region is correct in `alfa.tf`  
  
5. Edit `beta.tf` and change the bucket name to the bucket you created in step 3. Also ensure the region is correct.  
  
6. Edit `sample.tf` and include your bucket name from step 3. This file requires two modules taken from the terraform module registry here: https://registry.terraform.io/modules/mewa, edited for terraform 0.12. They are included in this example.  
  
### Spinning and Heaving  
  
If everything is configured, and your s3 bucket is created (see above), you are ready to Spin &amp; heave. Since you will need to init terraform, you should run `spin-and-heave` with the `-i (--init)` flag.  
  
```
$ spin-and-heave -i sample
```  
  
Any change you make to `sample/sample.py` that you wanted to publish, you'd simply run `spin-and-heave` again.  
  
```
$ spin-and-heave sample
```  
