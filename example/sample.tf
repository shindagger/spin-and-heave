locals {
  sample_lambda_runtime = "python3.6"
  sample_zip_name = "sample.zip"
  sample_project_zip = "${path.cwd}/sample.zip"
  sample_lambda_bucket = "andy-klier-zuar"
  sample_lambda_name = "sah-sample"
  sample_lambda_handler = "sample.handler"
  sample_lambda_log_name = "sample-lambda-logging"
  sample_lambda_role_name = "sah-sample-lambda-function-lambda-role"
  sample_apigw_name = "sah-sample-api-gw"
  sample_usage_plan_name = "sah-sample-usage-plan"
  sample_api_key_name = "sah-sample-key"
}

resource "aws_s3_bucket_object" "sample-lambda_object" {
  bucket = local.sample_lambda_bucket
  key    = local.sample_zip_name
  source = local.sample_zip_name
  etag = filemd5(local.sample_project_zip)
}

# LAMBDA FUNCTION

resource "aws_lambda_function" "sample-lambda-function" {
  function_name = local.sample_lambda_name
  s3_bucket = local.sample_lambda_bucket
  s3_key    = local.sample_zip_name
  handler = local.sample_lambda_handler
  runtime = local.sample_lambda_runtime
  memory_size = 256
  timeout     = 30
  role = aws_iam_role.sample-lambda_exec.arn
  source_code_hash = filebase64sha256(local.sample_project_zip)
  depends_on    = [
    aws_iam_role_policy_attachment.sample-lambda_logs,
    aws_cloudwatch_log_group.sample-lambda-function-log-group,
    aws_s3_bucket_object.sample-lambda_object]
}

# IAM role which dictates what other AWS services the Lambda function
# may access.
resource "aws_iam_role" "sample-lambda_exec" {
  name = local.sample_lambda_role_name
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {   
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },  
      "Effect": "Allow",
      "Sid": ""
    }   
  ]
}
EOF
}

# CLOUDWATCH LOG GROUP

resource "aws_cloudwatch_log_group" "sample-lambda-function-log-group" {
  name              = "/aws/lambda/${local.sample_lambda_name}"
  retention_in_days = 14
}

resource "aws_iam_policy" "sample-lambda_logging" {
  name = local.sample_lambda_log_name
  path = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "sample-lambda_logs" {
  role = aws_iam_role.sample-lambda_exec.name
  policy_arn = aws_iam_policy.sample-lambda_logging.arn
  depends_on = [
    aws_iam_role.sample-lambda_exec
  ]
}

# API GATEWAY

resource "aws_api_gateway_rest_api" "sample-api" {
  name = local.sample_apigw_name
}

# API KEY/USAGE PLAN

resource "aws_api_gateway_usage_plan" "sample-usage_plan" {
  name = local.sample_usage_plan_name
  api_stages {
    api_id = aws_api_gateway_rest_api.sample-api.id
    stage  = "api"
  }
  quota_settings {
    limit  = 5000
    period = "MONTH"
  }
  throttle_settings {
    burst_limit = 200
    rate_limit  = 100
  }
  depends_on = [
    aws_api_gateway_rest_api.sample-api,
    aws_api_gateway_deployment.sample-deploy,
  ]
}

# api key

resource "aws_api_gateway_api_key" "sample-key" {
  name = local.sample_api_key_name
}

resource "aws_api_gateway_usage_plan_key" "sample-plan_key" {
  key_id        = aws_api_gateway_api_key.sample-key.id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.sample-usage_plan.id
  depends_on    = [aws_api_gateway_usage_plan.sample-usage_plan]
}

# CUSTOMIZE ROUTES BELOW

module "sample-route" {
  source        = "./serverless-resource"
  api           = aws_api_gateway_rest_api.sample-api.id
  root_resource = aws_api_gateway_rest_api.sample-api.root_resource_id
  api_key_required = true
  resource      = "sample"
  num_methods   = 1
  methods = [
    {
      method     = "GET"
      type       = "AWS_PROXY"
      invoke_arn = aws_lambda_function.sample-lambda-function.invoke_arn
    },
  ]
}

module "sample-slug" {
  source        = "./serverless-resource"
  api           = aws_api_gateway_rest_api.sample-api.id
  root_resource = module.sample-route.resource
  api_key_required = true
  resource      = "{slug}"
  num_methods   = 1
  methods = [
    {
      method     = "GET"
      type       = "AWS_PROXY"
      invoke_arn = aws_lambda_function.sample-lambda-function.invoke_arn
    },
  ]
}

# DEPLOYMENT

resource "aws_api_gateway_deployment" "sample-deploy" {
  depends_on  = [module.sample-slug]
  rest_api_id = aws_api_gateway_rest_api.sample-api.id
  stage_name  = "api"
  variables = {
    deploy_version = "0.0.1"
  }
}

# PERMISSION FOR API/LAMBDA

resource "aws_lambda_permission" "sample-apigw" {
  statement_id  = "Allow-sample-sample-apiInvoke"
  action        = "lambda:InvokeFunction"
  function_name = local.sample_lambda_name
  principal     = "apigateway.amazonaws.com"
  depends_on = [aws_lambda_function.sample-lambda-function]
  source_arn = "${aws_api_gateway_rest_api.sample-api.execution_arn}/*/*/*"
}

# OUTPUTS

output "sample-endpoint" {
  value = aws_api_gateway_deployment.sample-deploy.invoke_url
}

output "sample-api_key" {
  value = aws_api_gateway_api_key.sample-key.value
}
